package ch.verttigo.bEvent.Commands;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setSlotCommand implements CommandExecutor {
    private static void setSlot(int slot) {
        bEvent.slotSize = slot;
        bEvent.getPlugin().getConfig().set("slot", slot);
        bEvent.getPlugin().saveConfig();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (command.getName().equalsIgnoreCase("setslot")) {
            if (!(sender instanceof Player p)) {
                System.out.println("Tu es la console mongole.");
            } else if (p.hasPermission("bEvent.admin")) {
                if (args.length != 1) {
                    p.sendMessage("usage : /setslot <number>");
                } else {
                    int slotNumber = Integer.parseInt(args[0]);
                    setSlot(slotNumber);
                    p.sendMessage("Slot set to " + slotNumber);
                }
            }
        }
        return true;
    }

}
