package ch.verttigo.bEvent.Commands;

import ch.verttigo.bEvent.Task.removeAfk;
import ch.verttigo.bEvent.Utils.utils;
import ch.verttigo.bEvent.bEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.title.Title;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.time.Duration;
import java.util.UUID;

public class startCommand implements CommandExecutor {

    private int task;
    private int count;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("start")) {
            if (!(sender instanceof Player p)) {
                System.out.println("Tu es la console mongole.");
            } else {
                if (p.hasPermission("bEvent.admin")) {
                    count = 5;

                    task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(bEvent.getPlugin(), () -> {

                        //Title
                        final Title.Times times = Title.Times.of(Duration.ofMillis(200), Duration.ofMillis(700), Duration.ofMillis(200));
                        final Title countDown = Title.title(Component.text(count).color(NamedTextColor.RED), Component.empty(), times);
                        final Title go = Title.title(Component.text("GO !!!!!!").color(NamedTextColor.AQUA), Component.empty(), times);

                        for (Player all : Bukkit.getOnlinePlayers()) all.showTitle(countDown);

                        if (count == 0) {
                            Bukkit.getScheduler().cancelTask(task);

                            //Start
                            bEvent.hasEventStarted = true;
                            removeAfk.startAFKSchedule();

                            //GO
                            for (Player all : Bukkit.getOnlinePlayers()) all.showTitle(go);

                            //TP Les joueurs
                            for (UUID uuid : bEvent.playerInEvent) {
                                Player playerInEvent = Bukkit.getPlayer(uuid);
                                playerInEvent.teleport(utils.getSpawnLocation());
                                playerInEvent.setGameMode(GameMode.SURVIVAL);

                            }
                        }

                        count--;

                    }, 0, 20);

                }
            }
        }
        return true;
    }
}
