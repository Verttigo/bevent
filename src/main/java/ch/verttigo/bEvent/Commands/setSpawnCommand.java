package ch.verttigo.bEvent.Commands;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setSpawnCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("setspawn")) {
            if (!(sender instanceof Player p)) {
                System.out.println("Tu es la console mongole.");
            } else {
                if (p.hasPermission("bEvent.admin")) {
                    Location loc = p.getLocation();
                    bEvent.getPlugin().getConfig().set("spawnX", loc.getX());
                    bEvent.getPlugin().getConfig().set("spawnY", loc.getY());
                    bEvent.getPlugin().getConfig().set("spawnZ", loc.getZ());
                    bEvent.getPlugin().getConfig().set("spawnYaw", loc.getYaw());
                    bEvent.getPlugin().getConfig().set("spawnPitch", loc.getPitch());
                    bEvent.getPlugin().saveConfig();
                    p.sendMessage("Spawn point set");
                }
            }
        }
        return true;
    }
}
