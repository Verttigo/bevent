package ch.verttigo.bEvent.Task;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.Bukkit;

import java.util.Map;
import java.util.UUID;

public class removeAfk {

    public static void startAFKSchedule() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(bEvent.getPlugin(), () -> {
            for (Map.Entry<UUID, Long> entry : bEvent.disconnectedPlayers.entrySet()) {
                UUID key = entry.getKey();
                long value = entry.getValue();
                long diff = System.currentTimeMillis() - value;
                if (diff > (1000 * 15)) {
                    bEvent.disconnectedPlayers.remove(key);
                    bEvent.playerInEvent.remove(key);
                }
            }
        }, 0, 20);
    }

}
