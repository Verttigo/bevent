package ch.verttigo.bEvent.Utils;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class utils {

    public static Location getSpawnLocation() {
        double yaw = bEvent.getPlugin().getConfig().getDouble("spawnYaw");
        double pitch = bEvent.getPlugin().getConfig().getDouble("spawnPitch");

        return new Location(Bukkit.getWorld("world"),
                bEvent.getPlugin().getConfig().getDouble("spawnX"),
                bEvent.getPlugin().getConfig().getDouble("spawnY"),
                bEvent.getPlugin().getConfig().getDouble("spawnZ"),
                (float) yaw,
                (float) pitch);
    }
}
