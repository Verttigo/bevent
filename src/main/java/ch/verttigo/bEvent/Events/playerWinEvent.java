package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.Scoreboard.sbManager;
import ch.verttigo.bEvent.bEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.title.Title;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import java.time.Duration;
import java.util.Objects;

public class playerWinEvent implements Listener {

    @EventHandler
    public static void onPlayerWin(PlayerInteractEvent event) {
        Player p = event.getPlayer();

        if (event.getAction().equals(Action.PHYSICAL) &&
                Objects.requireNonNull(event.getClickedBlock()).getType() == Material.LIGHT_WEIGHTED_PRESSURE_PLATE &&
                bEvent.hasEventStarted &&
                !p.hasPermission("bEvent.admin") &&
                bEvent.playerInEvent.contains(p.getUniqueId())) {

            final Title.Times times = Title.Times.of(Duration.ofMillis(500), Duration.ofMillis(5000), Duration.ofMillis(1000));
            final Title title = Title.title(Component.text(p.getName() + " a gagné l'event !"), Component.empty(), times);

            bEvent.playerInEvent.clear();

            //firework win !

            Firework fw = p.getWorld().spawn(p.getLocation(), Firework.class);
            FireworkMeta fwm = fw.getFireworkMeta();
            Builder builder = FireworkEffect.builder();

            fwm.addEffect(builder.flicker(true).withColor(Color.BLUE).build());
            fwm.addEffect(builder.trail(true).build());
            fwm.addEffect(builder.withFade(Color.ORANGE).build());
            fwm.addEffect(builder.with(Type.CREEPER).build());
            fwm.setPower(2);
            fw.setFireworkMeta(fwm);

            for (Player playerIn : Bukkit.getOnlinePlayers()) {
                sbManager.removeViewer(playerIn);
                playerIn.showTitle(title);

                if (!playerIn.hasPermission("bEvent.admin")) {
                    playerIn.setGameMode(GameMode.SPECTATOR);
                }
            }
        }
    }
}
