package ch.verttigo.bEvent.Events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class loginEvent implements Listener {

    @EventHandler
    public void OnPlayerJoin(PlayerLoginEvent e) {
        if (e.getResult() == PlayerLoginEvent.Result.KICK_FULL) {
            if (e.getPlayer().hasPermission("bEvent.admin")) {
                e.allow();
            } else {
                int numberOfOP = (int) Bukkit.getOnlinePlayers().stream().filter(p -> p.hasPermission("bEvent.admin")).count();
                int onlinePlayers = Bukkit.getOnlinePlayers().size();
                int maxPlayer = Bukkit.getServer().getMaxPlayers();
                if (maxPlayer > (onlinePlayers - numberOfOP)) {
                    e.allow();
                }
            }
        }
    }
}
