package ch.verttigo.bEvent.Events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class chatEvent implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent evt) {
        final Player p = evt.getPlayer();
        if (!p.hasPermission("bEvent.admin")) {
            evt.setCancelled(true);
        } else {
            evt.setFormat(ChatColor.translateAlternateColorCodes('&',
                    "&c[OPERATEUR]" + " &3" + p.getName() + ": &a" + evt.getMessage()));
        }

    }
}
