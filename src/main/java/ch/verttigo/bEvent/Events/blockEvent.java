package ch.verttigo.bEvent.Events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class blockEvent implements Listener {

    @EventHandler
    public static void onBreakEvent(BlockBreakEvent event) {
        Player p = event.getPlayer();
        if (!p.hasPermission("bEvent.admin")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public static void onBoatPlace(BlockPlaceEvent event) {
        Player p = event.getPlayer();
        if (!p.hasPermission("bEvent.admin")) {
            if (event.getBlockPlaced().getType() != Material.OAK_BOAT) {
                event.setCancelled(true);
            }
        }
    }
}
