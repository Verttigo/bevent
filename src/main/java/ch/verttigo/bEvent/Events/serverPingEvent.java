package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class serverPingEvent implements Listener {

    @EventHandler
    public void serverListPingEvent(ServerListPingEvent event) {
        event.setMaxPlayers(bEvent.slotSize);
    }
}
