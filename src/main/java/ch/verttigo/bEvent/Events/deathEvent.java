package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class deathEvent implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        Player p = e.getEntity();
        e.getDrops().clear();

        if (!p.hasPermission("bEvent.admin") && bEvent.hasEventStarted && bEvent.playerInEvent.contains(p.getUniqueId())) {
            p.setGameMode(GameMode.SPECTATOR);
            bEvent.playerInEvent.remove(p.getUniqueId());
        }
    }
}
