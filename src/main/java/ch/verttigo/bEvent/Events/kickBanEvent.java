package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.Scoreboard.sbManager;
import ch.verttigo.bEvent.bEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class kickBanEvent implements Listener {

    @EventHandler
    public static void onKickEvent(PlayerKickEvent event) {
        Player p = event.getPlayer();
        sbManager.removeViewer(p);
        //If Player is not op and if event has started and if the player is in the game
        if (!p.hasPermission("bEvent.admin") && bEvent.hasEventStarted) {
            bEvent.playerInEvent.remove(p.getUniqueId());
        }
    }

    @EventHandler
    public static void onBanEvent(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        sbManager.removeViewer(p);
        //If Player is not op and if event has started and if the player is in the game
        if (!p.hasPermission("bEvent.admin") && bEvent.hasEventStarted && bEvent.playerInEvent.contains(p.getUniqueId()) && p.isBanned()) {
            bEvent.playerInEvent.remove(p.getUniqueId());
        }
    }
}
