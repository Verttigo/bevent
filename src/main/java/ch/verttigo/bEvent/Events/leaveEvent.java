package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.bEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class leaveEvent implements Listener {

    @EventHandler
    public static void onLeaveEvent(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Player p = event.getPlayer();

        //If Player is not op and if event has started and if the player is in the game
        if (!p.hasPermission("bEvent.admin")) {
            if (bEvent.playerInEvent.contains(p.getUniqueId())) {
                if (bEvent.hasEventStarted) {
                    bEvent.disconnectedPlayers.put(p.getUniqueId(), System.currentTimeMillis());
                } else {
                    bEvent.playerInEvent.remove(p.getUniqueId());
                }
            }
        }
    }
}
