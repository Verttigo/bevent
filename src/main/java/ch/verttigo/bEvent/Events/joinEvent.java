package ch.verttigo.bEvent.Events;

import ch.verttigo.bEvent.Scoreboard.sbManager;
import ch.verttigo.bEvent.Utils.utils;
import ch.verttigo.bEvent.bEvent;
import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

public class joinEvent implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();

        //SB
        sbManager.addViewer(p);

        //food, sat, health
        p.setSaturation(40);
        p.setFoodLevel(20);
        p.setHealth(20.0);

        //Playerlist
        final Component header = Component.text("Escalader la montagne pour gagner !");
        final Component footer = Component.text("Powered by : baptiste.it");
        p.sendPlayerListHeaderAndFooter(header, footer);

        //If Player is not op
        if (!p.hasPermission("bEvent.admin")) {
            event.setJoinMessage(null);
            p.getInventory().clear();

            //Remove potion effects
            for (PotionEffect effect : p.getActivePotionEffects()) {
                p.removePotionEffect(effect.getType());
            }

            //If event is started
            if (bEvent.hasEventStarted) {
                //if the player is not in the game
                if (!bEvent.playerInEvent.contains(event.getPlayer().getUniqueId())) {
                    p.sendMessage("Tu es en spectateur");
                    p.setGameMode(GameMode.SPECTATOR);
                    p.teleport(utils.getSpawnLocation());
                }
            } else {
                //Put in the array
                bEvent.playerInEvent.add(event.getPlayer().getUniqueId());
                p.setGameMode(GameMode.SURVIVAL);
                p.teleport(utils.getSpawnLocation());
            }

        } else {
            //Annonce in the chat
            event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', "&6Event &7>> &6" + p.getName() + " &ea rejoint l'event!"));
        }
    }
}
