package ch.verttigo.bEvent.Scoreboard;

import ch.verttigo.bEvent.bEvent;
import me.catcoder.sidebar.ProtocolSidebar;
import me.catcoder.sidebar.Sidebar;
import me.catcoder.sidebar.text.TextIterators;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.entity.Player;

public class sbManager {

    private static final Sidebar<Component> sidebar = ProtocolSidebar.newAdventureSidebar(TextIterators.textFadeHypixel("Minecraft BriceBBrice"), bEvent.getPlugin());

    public static void createScoreboard() {
        sidebar.addBlankLine();
        sidebar.addUpdatableLine(player -> Component.text("Nbr de joueurs en vie : ").append(Component.text(bEvent.playerInEvent.size()).color(NamedTextColor.GREEN)));
        sidebar.addBlankLine();
        sidebar.addConditionalLine(
                player -> Component.text("L'event va commencer..."),
                player -> !bEvent.hasEventStarted
        );
        sidebar.addConditionalLine(
                player -> Component.text("Event en cours !"),
                player -> bEvent.hasEventStarted
        );
        sidebar.addBlankLine();
        sidebar.addLine(Component.text("Bonne chance !").color(NamedTextColor.YELLOW));
        sidebar.updateLinesPeriodically(0, 20);
    }

    public static void addViewer(Player p) {
        sidebar.addViewer(p);
    }

    public static void removeViewer(Player p) {
        sidebar.removeViewer(p);
    }


}
