package ch.verttigo.bEvent;

import ch.verttigo.bEvent.Commands.setSlotCommand;
import ch.verttigo.bEvent.Commands.setSpawnCommand;
import ch.verttigo.bEvent.Commands.startCommand;
import ch.verttigo.bEvent.Events.*;
import ch.verttigo.bEvent.Scoreboard.sbManager;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class bEvent extends JavaPlugin implements Listener {


    public static bEvent plugin;
    public static HashMap<UUID, Long> disconnectedPlayers = new HashMap<>();
    public static ArrayList<UUID> playerInEvent = new ArrayList<>();
    public static Boolean hasEventStarted = false;
    public static int slotSize = 0;
    private static boolean firstTime = false;

    public static bEvent getPlugin() {
        return plugin;
    }

    @Override
    public void onLoad() {
        firstTime = true;
    }

    @Override
    public void onEnable() {

        //reload detection
        if (!firstTime) System.out.println("The plugin has been reloaded, the plugin will not work has intended !!!");

        //init instance
        plugin = this;

        //init Config
        this.saveDefaultConfig();

        //Init scoreboad
        sbManager.createScoreboard();

        //Init events
        this.getServer().getPluginManager().registerEvents(new deathEvent(), this);
        this.getServer().getPluginManager().registerEvents(new loginEvent(), this);
        this.getServer().getPluginManager().registerEvents(new joinEvent(), this);
        this.getServer().getPluginManager().registerEvents(new kickBanEvent(), this);
        this.getServer().getPluginManager().registerEvents(new leaveEvent(), this);
        this.getServer().getPluginManager().registerEvents(new chatEvent(), this);
        this.getServer().getPluginManager().registerEvents(new blockEvent(), this);
        this.getServer().getPluginManager().registerEvents(new playerWinEvent(), this);
        this.getServer().getPluginManager().registerEvents(new playerPVPEvent(), this);
        this.getServer().getPluginManager().registerEvents(new utilsEvent(), this);
        this.getServer().getPluginManager().registerEvents(new playerFallEvent(), this);
        this.getServer().getPluginManager().registerEvents(new serverPingEvent(), this);

        //init commands
        this.getCommand("setspawn").setExecutor(new setSpawnCommand());
        this.getCommand("setslot").setExecutor(new setSlotCommand());
        this.getCommand("start").setExecutor(new startCommand());

        //init slot size in memory to prevent endless access to config
        slotSize = this.getConfig().getInt("slot");

    }

    @Override
    public void onDisable() {
        firstTime = false;
    }

}